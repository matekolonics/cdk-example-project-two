import * as cdk from 'aws-cdk-lib';
import {
  aws_autoscaling,
  aws_ec2,
  aws_elasticloadbalancingv2,
  aws_rds,
  aws_route53,
  aws_s3, RemovalPolicy,
  SecretValue
} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {AmazonLinuxCpuType, InstanceClass, InstanceSize, InstanceType, Peer, Port} from "aws-cdk-lib/aws-ec2";
import {Credentials, DatabaseInstanceEngine} from "aws-cdk-lib/aws-rds";
import {ApplicationProtocol} from "aws-cdk-lib/aws-elasticloadbalancingv2";
import {DnsValidatedCertificate} from "aws-cdk-lib/aws-certificatemanager";
import {HealthCheck} from "aws-cdk-lib/aws-autoscaling";

export class CdkExampleAutoScalingStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new aws_ec2.Vpc(this, 'exampleVPC', {});

    const securityGroup = new aws_ec2.SecurityGroup(this, 'exampleSG', {
      vpc,
    });
    securityGroup.addIngressRule(Peer.ipv4('0.0.0.0/0'), Port.allTraffic());

    const dnsZone = aws_route53.HostedZone.fromLookup(this, 'dnsZone', {
      domainName: 'example.com',
    });
    const sslCertificate = new DnsValidatedCertificate(this, 'exampleCertificate', {
      domainName: 'example.com',
      hostedZone: dnsZone,
      subjectAlternativeNames: [ '*.example.com' ],
    });

    const loadBalancer = new aws_elasticloadbalancingv2.ApplicationLoadBalancer(this, 'exampleLB', {
      vpc,
    });
    const httpsListener = loadBalancer.addListener('exampleListener', {
      certificates: [ sslCertificate ],
      protocol: ApplicationProtocol.HTTPS,
      port: 443,
    });

    const ami = aws_ec2.MachineImage.latestAmazonLinux2023({
      cpuType: AmazonLinuxCpuType.ARM_64,
    });

    const autoScalingGroup = new aws_autoscaling.AutoScalingGroup(this, 'exampleASG', {
      vpc,
      instanceType: InstanceType.of(InstanceClass.T4G, InstanceSize.SMALL),
      machineImage: ami,
      healthCheck: HealthCheck.ec2(),
    });

    /* This is how you can define an init script.
       These commands will be executed on the server once it's started. */
    autoScalingGroup.addUserData(
        //e.g. update packages
        'yum update',
    );

    // Attach the auto-scaling group to the load balancer.
    httpsListener.addTargets('exampleTargetGroup', {
      port: 443,
      protocol: ApplicationProtocol.HTTPS,
      targets: [ autoScalingGroup ],
      healthCheck: {
        // This is the endpoint on the server that will be pinged by the ALB health check.
        path: '/health',
        port: '443',
        healthyHttpCodes: '200',
      }
    })

    const rds = new aws_rds.DatabaseInstance(this, 'ExampleDB', {
      vpc,
      engine: DatabaseInstanceEngine.POSTGRES,
      instanceType: InstanceType.of(
          InstanceClass.T4G,
          InstanceSize.MICRO,
      ),
      databaseName: 'example',
      credentials: Credentials.fromPassword('root', SecretValue.unsafePlainText('Test1234')),
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const s3Bucket = new aws_s3.Bucket(this, 'ExampleBucket', {});
  }
}
