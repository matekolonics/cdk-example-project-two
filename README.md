# CDK Example Project - Auto-Scaling
This is a more advanced version of the [example webapp infrastructure](https://gitlab.com/matekolonics/cdk-example-project-one) implemented in AWS CDK. This project does not contain a CloudFormation template, because a valid Route 53 hosted zone would be needed for the CloudFormation template to be successfully generated (I used `example.com` in the project). If you would like to check out CloudFormation, the [other project](https://gitlab.com/matekolonics/cdk-example-project-one) has one. \
Resources defined in this stack:
- EC2 auto-scaling group (managing servers for running code)
- Load balancer (distributing traffic between the ASG nodes)
- Route 53 domain name
- SSL certificate
- RDS instance (database)
- S3 bucket (for storing files)
# Working with CDK
## Creating a new CDK Project
### 1. Install CDK
```bash
npm i -g aws-cdk
```
### 2. Bootstrap CDK environment
```bash
cdk bootstrap aws://ACCOUNT-NUMBER/REGION
```
### 3. Create project directory
```bash
mkdir cdk-example-project
cd cdk-example-project
```
### 4. Initialize CDK app
```bash
cdk init app --language typescript
```
## Generating a CloudFormation template
Running the following command will generate the template and output it in the terminal:
```bash
cdk synth
```
\
 If you would like to save it to a file instead, run the following command:
 ```bash
 cdk synth > cloudformation.yaml
 ```
 \
 Now you can deploy this template file using CloudFormation.
 ## Deploying using CDK CLI
 You can also deploy your CDK project without CloudFormation (`cdk synth` is not needed if you choose this method).
 To do so, run the following command:
 ```bash
 cdk deploy
 ``` 
